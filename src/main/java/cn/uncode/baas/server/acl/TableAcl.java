package cn.uncode.baas.server.acl;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.utils.JsonUtils;

public class TableAcl implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 2135756349801970386L;

    public static final String READ = "read";
    public static final String WRITE = "write";
    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String REMOVE = "remove";

    private Acl read = new Acl();

    private Acl write = new Acl();

    private Acl insert = new Acl();

    private Acl update = new Acl();

    private Acl remove = new Acl();

    public Acl getRead() {
        return read;
    }

    public void setRead(Acl read) {
        this.read = read;
    }

    public Acl getWrite() {
        return write;
    }

    public void setWrite(Acl write) {
        this.write = write;
    }

    public Acl getInsert() {
        return insert;
    }

    public void setInsert(Acl insert) {
        this.insert = insert;
    }

    public Acl getRemove() {
        return remove;
    }

    public void setRemove(Acl remove) {
        this.remove = remove;
    }

    public Acl getUpdate() {
        return update;
    }

    public void setUpdate(Acl update) {
        this.update = update;
    }

    public void fromJson(String value) {
        if (StringUtils.isNotBlank(value)) {
            Map<?, ?> valueMap = JsonUtils.fromJson(value, Map.class);
            if (valueMap != null) {
            	if (valueMap.containsKey("read") && valueMap.get("read")!=null) {
            		Map<?, ?> valueObj = (Map<?, ?>) valueMap.get("read");
            		if (null != valueObj && !valueObj.isEmpty()) {
            			this.read.fromMap(valueObj);
            		}
            	}
            	if (valueMap.containsKey("write") && valueMap.get("write")!=null) {
            		Map<?, ?> valueObj = (Map<?, ?>) valueMap.get("write");
            		if (null != valueObj && !valueObj.isEmpty()) {
            			this.read.fromMap(valueObj);
            		}
            	}
            	if (valueMap.containsKey("insert") && valueMap.get("insert")!=null) {
            		Map<?, ?> valueObj = (Map<?, ?>) valueMap.get("insert");
            		if (null != valueObj && !valueObj.isEmpty()) {
            			this.read.fromMap(valueObj);
            		}
            	}
            	if (valueMap.containsKey("update") && valueMap.get("update")!=null) {
            		Map<?, ?> valueObj = (Map<?, ?>) valueMap.get("update");
            		if (null != valueObj && !valueObj.isEmpty()) {
            			this.read.fromMap(valueObj);
            		}
            	}
            	if (valueMap.containsKey("remove") && valueMap.get("remove")!=null) {
            		Map<?, ?> valueObj = (Map<?, ?>) valueMap.get("remove");
            		if (null != valueObj && !valueObj.isEmpty()) {
            			this.read.fromMap(valueObj);
            		}
            	}
			}
        }
    }

    public String toJson() {
        return JsonUtils.objToJson(this);
    }

    public String toString() {
        return JsonUtils.objToJson(this);
    }

}
