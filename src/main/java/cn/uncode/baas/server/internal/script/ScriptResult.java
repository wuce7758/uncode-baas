package cn.uncode.baas.server.internal.script;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.baas.server.utils.DataUtils;

public class ScriptResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5378173514986270250L;
	
	public static final String LOCATION = "location";
	
	private Object result;
	
	private String err;
	
	private String logValue;
	
	public ScriptResult() {}
	
	public ScriptResult(Object result, String err) {
		this.result = result;
		this.err = err;
	}

	public Object getResponse() {
		if(StringUtils.isNotEmpty(logValue) && null != result){
			Map<String, Object> resultTemp = DataUtils.convert2Map(result);
			if(null != resultTemp){
				resultTemp.put("logs", logValue);
			}
			return resultTemp;
		}
		return result;
	}
	
	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		if(StringUtils.isNotEmpty(err) && !"null".equals(err)){
			this.err = err;
		}
	}
	
	public String getLocation(){
		String id = null;
		return id;
	}

	public void setLogValue(String logValue) {
		this.logValue = logValue;
	}

	
	
	

}
