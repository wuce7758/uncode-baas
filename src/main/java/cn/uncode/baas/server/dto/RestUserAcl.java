package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.Map;

public class RestUserAcl  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7677751002702461674L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String USER_NAME = "username";
	public static final String GROUPS = "groups";
	public static final String ROLES = "roles";
	
	private int id;
	private String bucket;
	private String username;
	private String groups;
	private String roles;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getGroups() {
		return groups;
	}
	public void setGroups(String groups) {
		this.groups = groups;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	
	
	public static RestUserAcl valueOf(Map<String, Object> map){
		RestUserAcl restUserAcl = null;
		if(null != map){
			restUserAcl = new RestUserAcl();
			if(map.containsKey(ID)){
				restUserAcl.setId((Integer)map.get(ID));
			}
			if(map.containsKey(BUCKET)){
				restUserAcl.setBucket(String.valueOf(map.get(BUCKET)));
			}
			if(map.containsKey(USER_NAME)){
				restUserAcl.setUsername(String.valueOf(map.get(USER_NAME)));
			}
			if(map.containsKey(GROUPS)){
				restUserAcl.setGroups(String.valueOf(map.get(GROUPS)));
			}
			if(map.containsKey(ROLES)){
				restUserAcl.setRoles(String.valueOf(map.get(ROLES)));
			}
		}
		return restUserAcl;
	}
	

}
